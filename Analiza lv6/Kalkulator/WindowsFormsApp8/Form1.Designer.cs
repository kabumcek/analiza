﻿namespace WindowsFormsApp8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_a = new System.Windows.Forms.TextBox();
            this.tb_b = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Izlaz = new System.Windows.Forms.Button();
            this.btn_zbroji = new System.Windows.Forms.Button();
            this.btn_djeli = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_oduzmi = new System.Windows.Forms.Button();
            this.btn_pomnozi = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.btn_sqrt = new System.Windows.Forms.Button();
            this.btn_kvadrat = new System.Windows.Forms.Button();
            this.tb_rj = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_napr = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(78, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Broj1:";
            // 
            // tb_a
            // 
            this.tb_a.Location = new System.Drawing.Point(174, 180);
            this.tb_a.Name = "tb_a";
            this.tb_a.Size = new System.Drawing.Size(100, 20);
            this.tb_a.TabIndex = 1;
            // 
            // tb_b
            // 
            this.tb_b.Location = new System.Drawing.Point(174, 223);
            this.tb_b.Name = "tb_b";
            this.tb_b.Size = new System.Drawing.Size(100, 20);
            this.tb_b.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(78, 225);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Broj2:";
            // 
            // btn_Izlaz
            // 
            this.btn_Izlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Izlaz.Location = new System.Drawing.Point(540, 392);
            this.btn_Izlaz.Name = "btn_Izlaz";
            this.btn_Izlaz.Size = new System.Drawing.Size(93, 46);
            this.btn_Izlaz.TabIndex = 4;
            this.btn_Izlaz.Text = "Izlaz";
            this.btn_Izlaz.UseVisualStyleBackColor = true;
            this.btn_Izlaz.Click += new System.EventHandler(this.btn_Izlaz_Click);
            // 
            // btn_zbroji
            // 
            this.btn_zbroji.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_zbroji.Location = new System.Drawing.Point(63, 278);
            this.btn_zbroji.Name = "btn_zbroji";
            this.btn_zbroji.Size = new System.Drawing.Size(93, 34);
            this.btn_zbroji.TabIndex = 5;
            this.btn_zbroji.Text = "+";
            this.btn_zbroji.UseVisualStyleBackColor = true;
            this.btn_zbroji.Click += new System.EventHandler(this.btn_zbroji_Click);
            // 
            // btn_djeli
            // 
            this.btn_djeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_djeli.Location = new System.Drawing.Point(63, 334);
            this.btn_djeli.Name = "btn_djeli";
            this.btn_djeli.Size = new System.Drawing.Size(93, 32);
            this.btn_djeli.TabIndex = 8;
            this.btn_djeli.Text = "/";
            this.btn_djeli.UseVisualStyleBackColor = true;
            this.btn_djeli.Click += new System.EventHandler(this.btn_djeli_Click);
            // 
            // btn_sin
            // 
            this.btn_sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sin.Location = new System.Drawing.Point(63, 392);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(93, 34);
            this.btn_sin.TabIndex = 9;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.btn_sin_Click);
            // 
            // btn_cos
            // 
            this.btn_cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cos.Location = new System.Drawing.Point(181, 392);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(93, 34);
            this.btn_cos.TabIndex = 10;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.btn_cos_Click);
            // 
            // btn_oduzmi
            // 
            this.btn_oduzmi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_oduzmi.Location = new System.Drawing.Point(181, 278);
            this.btn_oduzmi.Name = "btn_oduzmi";
            this.btn_oduzmi.Size = new System.Drawing.Size(93, 34);
            this.btn_oduzmi.TabIndex = 11;
            this.btn_oduzmi.Text = "-";
            this.btn_oduzmi.UseVisualStyleBackColor = true;
            this.btn_oduzmi.Click += new System.EventHandler(this.btn_oduzmi_Click);
            // 
            // btn_pomnozi
            // 
            this.btn_pomnozi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_pomnozi.Location = new System.Drawing.Point(314, 278);
            this.btn_pomnozi.Name = "btn_pomnozi";
            this.btn_pomnozi.Size = new System.Drawing.Size(93, 34);
            this.btn_pomnozi.TabIndex = 12;
            this.btn_pomnozi.Text = "*";
            this.btn_pomnozi.UseVisualStyleBackColor = true;
            this.btn_pomnozi.Click += new System.EventHandler(this.btn_pomnozi_Click);
            // 
            // btn_log
            // 
            this.btn_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_log.Location = new System.Drawing.Point(314, 392);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(93, 34);
            this.btn_log.TabIndex = 13;
            this.btn_log.Text = "Log";
            this.btn_log.UseVisualStyleBackColor = true;
            this.btn_log.Click += new System.EventHandler(this.btn_log_Click);
            // 
            // btn_sqrt
            // 
            this.btn_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sqrt.Location = new System.Drawing.Point(314, 332);
            this.btn_sqrt.Name = "btn_sqrt";
            this.btn_sqrt.Size = new System.Drawing.Size(93, 34);
            this.btn_sqrt.TabIndex = 14;
            this.btn_sqrt.Text = "sqrt";
            this.btn_sqrt.UseVisualStyleBackColor = true;
            this.btn_sqrt.Click += new System.EventHandler(this.btn_sqrt_Click);
            // 
            // btn_kvadrat
            // 
            this.btn_kvadrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_kvadrat.Location = new System.Drawing.Point(181, 332);
            this.btn_kvadrat.Name = "btn_kvadrat";
            this.btn_kvadrat.Size = new System.Drawing.Size(93, 34);
            this.btn_kvadrat.TabIndex = 15;
            this.btn_kvadrat.Text = "y^2";
            this.btn_kvadrat.UseVisualStyleBackColor = true;
            this.btn_kvadrat.Click += new System.EventHandler(this.button9_Click);
            // 
            // tb_rj
            // 
            this.tb_rj.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_rj.Location = new System.Drawing.Point(63, 85);
            this.tb_rj.Name = "tb_rj";
            this.tb_rj.Size = new System.Drawing.Size(524, 40);
            this.tb_rj.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(311, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 18);
            this.label3.TabIndex = 17;
            this.label3.Text = "Broj(napredne operacije):";
            // 
            // tb_napr
            // 
            this.tb_napr.Location = new System.Drawing.Point(501, 200);
            this.tb_napr.Name = "tb_napr";
            this.tb_napr.Size = new System.Drawing.Size(100, 20);
            this.tb_napr.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 450);
            this.Controls.Add(this.tb_napr);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_rj);
            this.Controls.Add(this.btn_kvadrat);
            this.Controls.Add(this.btn_sqrt);
            this.Controls.Add(this.btn_log);
            this.Controls.Add(this.btn_pomnozi);
            this.Controls.Add(this.btn_oduzmi);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_djeli);
            this.Controls.Add(this.btn_zbroji);
            this.Controls.Add(this.btn_Izlaz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_b);
            this.Controls.Add(this.tb_a);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_a;
        private System.Windows.Forms.TextBox tb_b;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Izlaz;
        private System.Windows.Forms.Button btn_zbroji;
        private System.Windows.Forms.Button btn_djeli;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_oduzmi;
        private System.Windows.Forms.Button btn_pomnozi;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.Button btn_kvadrat;
        private System.Windows.Forms.TextBox tb_rj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_napr;
    }
}

