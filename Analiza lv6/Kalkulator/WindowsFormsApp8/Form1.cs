﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_zbroji_Click(object sender, EventArgs e)
        {
            double x = 0, y = 0;
            if(!double.TryParse(tb_a.Text, out x) || !double.TryParse(tb_b.Text,out y))
            {
                MessageBox.Show("jedan od brojeva nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = (x + y).ToString();
            }
        }

        private void btn_oduzmi_Click(object sender, EventArgs e)
        {
            double x = 0, y = 0;
            if (!double.TryParse(tb_a.Text, out x) || !double.TryParse(tb_b.Text, out y))
            {
                MessageBox.Show("jedan od brojeva nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = (x - y).ToString();
            }
        }

        private void btn_pomnozi_Click(object sender, EventArgs e)
        {
            double x = 0, y = 0;
            if (!double.TryParse(tb_a.Text, out x) || !double.TryParse(tb_b.Text, out y))
            {
                MessageBox.Show("jedan od brojeva nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = (x * y).ToString();
            }
        }

        private void btn_djeli_Click(object sender, EventArgs e)
        {
            double x = 0, y = 0;
            if (!double.TryParse(tb_a.Text, out x) || !double.TryParse(tb_b.Text, out y) || y==0)
            {
                MessageBox.Show("jedan od brojeva nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = (x/y).ToString();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            double x = 0;
            if(!double.TryParse(tb_napr.Text,out x))
            {
                MessageBox.Show("broj nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = Math.Pow(x,2).ToString();
            }
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            double x = 0;
            if (!double.TryParse(tb_napr.Text, out x))
            {
                MessageBox.Show("broj nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = Math.Sqrt(x).ToString();
            }
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            double x = 0;
            if (!double.TryParse(tb_napr.Text, out x))
            {
                MessageBox.Show("broj nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = Math.Sin(x).ToString();
            }
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            double x = 0;
            if (!double.TryParse(tb_napr.Text, out x))
            {
                MessageBox.Show("broj nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = Math.Cos(x).ToString();
            }
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            double x = 0;
            if (!double.TryParse(tb_napr.Text, out x))
            {
                MessageBox.Show("broj nije ispravno unesen");
            }
            else
            {
                tb_rj.Text = Math.Log(x).ToString();
            }
        }
    }
}
